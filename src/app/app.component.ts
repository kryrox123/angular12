import { Component } from '@angular/core';

const valores = ["A", "B", "C", "D", "E"]

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  opciones = [false, false, false, false, false]
  comprobado = false
  mensaje = ""
  selec = ""

  constructor() {
  }

  comprobar() {
    let lista = this.opciones.filter(valor => valor)
    if (lista.length === 0) {
      this.mensaje = "Seleccione una opcion"
      this.comprobado=true
      this.selec = ""
    }else if (lista.length === 1){
      this.comprobado = false
      this.selec = `Valor es: ${valores[this.opciones.indexOf(true)]}`
    } else {
      this.mensaje = "Solo puede elegir uan opcion"
      this.comprobado=true
      this.selec = ""
    }
  }
  
}
